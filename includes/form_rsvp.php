<?php	
	if(empty($_POST['guest_name']) && strlen($_POST['guest_name']) == 0 || empty($_POST['guest_email']) && strlen($_POST['guest_email']) == 0)
	{
		return false;
	}
	
	$guest_name = $_POST['guest_name'];
	$partner_name = $_POST['partner_name'];
	$community_ = $_POST['community_'];
	$guest_email = $_POST['guest_email'];
	$will_join = $_POST['will_join'];
	
	$to = 'faisalsashawedding@gmail.com'; // Email submissions are sent to this email

	// Create email	
	$email_subject = "RSVP";
	$email_body = "You have received a new guest. \n\n".
				  "Guest_Name: $guest_name \nPartner_Name: $partner_name \nCommunity_: $community_ \nGuest_Email: $guest_email \nWill_Join: $will_join \n";
	$headers = "MIME-Version: 1.0\r\nContent-type: text/plain; charset=UTF-8\r\n";	
	$headers .= "From: reservation@faisalsashawedding.site\n";
	$headers .= "Reply-To: $guest_email";	
	
	mail($to,$email_subject,$email_body,$headers); // Post message
	return true;			
?>